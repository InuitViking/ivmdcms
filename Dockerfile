FROM php:8.3-apache-bookworm

# Install dependencies
RUN apt -y update \
    && apt -y install rsync ssh git libzip-dev zlib1g zip unzip sudo \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

RUN pecl install zip-1.22.3 \
    && docker-php-ext-enable zip

WORKDIR /

# Install composer programmatically
RUN EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
RUN if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; then \
        rm composer-setup.php exit 1; \
    else \
        php composer-setup.php --quiet; \
        rm composer-setup.php; \
        chmod +x composer.phar; \
        mv composer.phar /usr/bin/composer; \
    fi

# Copy code to /var/www
COPY . /var/www

# Change default docroot
ENV APACHE_DOCUMENT_ROOT /var/www/web
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Enable necessary headers
RUN a2enmod rewrite headers cache cache_disk expires

# Make sure apache can do what it needs to do in /var/www
RUN chown -R www-data:www-data /var/www
RUN chmod -R g+rw /var/www/src/

# Set the default port to 80, but this is configurable
ENV APACHE_PORT 80
# Set the default max age of the apache cache in minutes; default is 15
ENV APACHE_CACHE_AGE 15
# Set the maximum size the cache is allowed to get before clearing; default is 5M
ENV APACHE_CACHE_SIZE 5M

# Ensure that the apache port is changed to the environment variable
RUN sed -i 's/80/${APACHE_PORT}/g' /etc/apache2/ports.conf
RUN sed -i 's/80/${APACHE_PORT}/g' /etc/apache2/sites-available/*.conf

# Setup some security things
RUN sed -i 's/ServerTokens OS/ServerTokens Prod/g' /etc/apache2/conf-available/security.conf
RUN sed -i 's/ServerSignature On/ServerSignature Off/g' /etc/apache2/conf-available/security.conf

# Enable cache to disk
RUN sed -i 's/#CacheEnable disk \//CacheEnable disk \//g' /etc/apache2/mods-available/cache_disk.conf
RUN sed -i 's/#CacheEnable disk \//CacheEnable disk \//g' /etc/apache2/mods-available/cache_disk.conf

# Configure CacheDirLevels
RUN sed -i 's/CacheDirLevels 2/CacheDirLevels 6/g' /etc/apache2/mods-available/cache_disk.conf
RUN sed -i 's/CacheDirLevels 1/CacheDirLevels 3/g' /etc/apache2/mods-available/cache_disk.conf

# Set the defaults for opcache in PHP
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="10000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="192" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10"

# Copy all the necessary files to the necessary directories
COPY .docker/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY .docker/php/conf.d/php_vars.ini /usr/local/etc/php/conf.d/php_vars.ini
COPY .docker/apache/apache2.conf /etc/apache2/apache2.conf

WORKDIR /var/www

# Necessary to get SSHD to work
RUN mkdir -p /run/sshd

ENV SSH_USER="imms" \
    SSH_PASS="imms" \
    SSH_PORT="2222"

RUN echo "ForceCommand internal-sftp" >> /etc/ssh/sshd_config

USER www-data
RUN /usr/bin/composer install

USER root
# Start htcacheclean and sshd as daemons and start apache in foreground
CMD if ! id -u "${SSH_USER}" >/dev/null 2>&1; then echo "Creating user $SSH_USER"; useradd -rd /var/www/src -s /bin/bash -g www-data "${SSH_USER}" -p $(openssl passwd -1 "${SSH_PASS}"); fi && sudo -u www-data /usr/bin/composer update && htcacheclean -d "$APACHE_CACHE_AGE" -l "$APACHE_CACHE_SIZE" -p/ && /usr/sbin/sshd -p "$SSH_PORT" && apachectl -D FOREGROUND