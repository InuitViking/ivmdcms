# IMMS - Inuitviking Markdown Management System

![pipeline status](https://gitlab.com/InuitViking/imms/badges/master/pipeline.svg)
![latest release](https://gitlab.com/inuitviking/imms/-/badges/release.svg?order_by=release_at&value_width=110)

[![Total Downloads](http://poser.pugx.org/inuitviking/imms/downloads)](https://packagist.org/packages/inuitviking/imms)
[![PHP Version Require](http://poser.pugx.org/inuitviking/imms/require/php)](https://packagist.org/packages/inuitviking/imms)
[![License](http://poser.pugx.org/inuitviking/imms/license)](https://packagist.org/packages/inuitviking/imms)

IMMS is a kind of SSG (Static Site Generator), but without you having to build it yourself and entirely without pipelines.

It takes your Markdown and renders it as HTML in a file cache, and serves this HTML without having to re-render it again.
This makes IMMS pretty fast at serving content.

If you have HTML files instead of Markdown files, it will simply stitch the HTML content together with overall template files, and cache the final result.
Again, making IMMS pretty fast at serving content.

If you need help or want to learn more, [please check out the wiki](https://gitlab.com/InuitViking/imms/-/wikis/home).

## Intended use cases

- To serve a repository with Markdown or HTML files (such as a GitHub/GitLab wiki!)
- To serve as an SFTP server for your old pals to host their static HTML (docker image supports this out of the box!)
- Other semi- to fully static content

There's quite literally no fuss (or very little of it):

- No database
- No users that need to log in
- No need for manual cache handling
- No built-in editors

## Features

- GitHub Flavoured Markdown
  - This is just the base; it should have almost full support for Gitlab Flavoured Markdown as well
- Header Permalinks
- Syntax highlighting for code blocks
- Hints
- Text Highlighting (Remember those yellow highlighters?)
- Youtube iframes
- Emojis
- Image attributes (size them however you want!)
- WikiLinks
- Sub pages
  - Create a directory with the same name of a Markdown file
  - Add markdown files into the directory
  - IMMS will link the Markdown file and directory together automatically.
- Support for special characters in markdown file name
- Table of Contents (configurable!)
- Automatic caching (and clearing of it!)
- A (albeit quite simple) CLI tool
- Lazy image loading
- Integrated git (this feature must be enabled in config)
- You can feed it HTML or Markdown; doesn't matter, it will handle it.
- Custom templates (using [Plates](https://github.com/thephpleague/plates)!)
- Custom plugins!
- Custom themes!
- Custom events!

## Additional features in Docker

- Makes use of PHP's opcache to improve performance
- Makes use of Apache's built-in caching, to serve files even faster

## Getting started

### Docker Compose

An example could be like so:
```yaml
services:
  imms:
    image: registry.gitlab.com/inuitviking/imms:latest
    ports:
      - 8080:80
      - 2222:2222
    #    expose:
    #      - 1234
    #      - 2222
    environment:
      - APACHE_PORT=1234
      - APACHE_CACHE_AGE=900
      - APACHE_CACHE_SIZE=5M
      - SSH_PORT=2222
      - SSH_USER=imms
      - SSH_PASS=imms
    volumes:
      - ./.docker/documents:/var/www/src/documents
      - ./.docker/config:/var/www/config
```

- `./.docker/documents`: The place where you store your uploads and markdown files.
- `./.docker/config`: This is where the `config.ini` file is stored

All of the above is supplied in `.docker` as examples.

You may also see the Apache config and PHP config within `.docker`; this is used for building the docker image, to configure opcache and Apache's built-in cache.

The docker image provided makes use of opcache and Apache's included caching system to improve response time.

If you prefer to use the built-in git, you can omit the "documents" volume, as IMMS would then handle the pulling internally.

### On a good ol' web server

```bash
composer create-project inuitviking/imms
cd imms
```

Once created, you can configure it to pull HTML/Markdown from a remote repository in `config/config.ini`.

If you prefer not to do that, make sure to create the necessary directories:

```bash
mkdir -p src/documents
touch src/documents/index.md
```

You may want to take a look at `config/config.ini` to adjust to your liking.

## Dependencies

**Composer dependencies:**

- [league/commonmark](https://github.com/thephpleague/commonmark)
- [league/plates](https://github.com/thephpleague/plates)
- [scssphp/scssphp](https://scssphp.github.io/scssphp/)
- [ueberdosis/commonmark-hint-extension](https://github.com/ueberdosis/commonmark-hint-extension)
- [zoon/commonmark-ext-youtube-iframe](https://github.com/zoonru/commonmark-ext-youtube-iframe)
- [elgigi/commonmark-emoji](https://github.com/ElGigi/CommonMarkEmoji)
- [n0sz/commonmark-marker-extension](https://github.com/noah1400/commonmark-marker-extension)
- [sven/commonmark-image-media-queries](https://github.com/svenluijten/commonmark-image-media-queries)
- [jnjxp/commonmark-wikilinks](https://github.com/jnjxp/jnjxp.commonmark-wikilinks)
- [simonvomeyser/commonmark-ext-lazy-image](https://github.com/simonvomeyser/commonmark-ext-lazy-image)
- [league/plates](https://github.com/thephpleague/plates)
- [czproject/git-php](https://github.com/czproject/git-php)
- [league/flysystem](https://github.com/thephpleague/flysystem)
- [thadbryson/flysystem-sync](https://github.com/thadbryson/flysystem-sync)

**PHP dependencies**

- PHP8.3 or newer
- ext-dom

**Server dependencies**

- [git](https://git-scm.com/)
- [composer](https://getcomposer.org/download/)
- [php](https://www.php.net/) (8.3 or newer)
  - ext-dom should also be installed
- A webserver
  - [Apache](https://httpd.apache.org/) with the php module
  - [NGINX](https://nginx.org/en/) with PHP FPM