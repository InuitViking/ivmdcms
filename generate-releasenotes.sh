#!/bin/bash
# Author:Andrey Nikishaev, Angutivik Casper Rúnur Tausen
echo "RELEASE NOTES"
echo ----------------------
git tag -l --sort=committerdate | tac | head -n 2 | while read -r TAG ; do
    if [ "$NEXT" ];then
        echo "## [$NEXT](https://gitlab.com/InuitViking/imms/-/releases/$NEXT)"
        GIT_PAGER=$(git log --no-merges --format=" * %s" "$TAG".."$NEXT")
        echo "$GIT_PAGER"
    fi
    NEXT=$TAG
done