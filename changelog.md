CHANGELOG
----------------------

## Unpublished
 * Added changelog

## [2024.05.07.22](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.22)
 * Added SpacedLinksProcessor

## [2024.05.07.21](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.21)
 * Try to fix pipeline
 * Try to fix pipeline
 * Try to fix pipeline
 * Try to fix pipeline

## [2024.05.07.20](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.20)
 * Change monolog to version 2
 * Change Monolog to version 2

## [2024.05.07.19](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.19)
 * Added Monolog everywhere
 * Added Monolog in one Class

## [2024.05.07.18](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.18)
 * Shift lastModified and now calculation

## [2024.05.07.17](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.17)
 * Added a new even

## [2024.05.07.16](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.16)
 * Fix issue where event listeners were loaded too late

## [2024.05.07.15](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.15)
 * Make use of one single MarkdownConverter

## [2024.05.07.14](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.14)
 * Revert Route.php back to its original state

## [2024.05.07.13](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.13)
 * Further speed up Route.

## [2024.05.07.12](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.12)
 * Simplify Theme.php and fix bug

## [2024.05.07.11](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.11)
 * Don't render SCSS every time

## [2024.05.07.10](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.10)
 * Synchronize gituploaddir to uploaddir on change
 * Simplify index.php

## [2024.05.07.9](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.9)
 * Improve load time when enabling built-in git

## [2024.05.07.8](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.8)
 * Fix composer being run as root in docker images
 * Update gitignore and remove comments

## [2024.05.07.7](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.7)

No unpublished changes


## [2024.05.07.6](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.6)
 * Updated PluginsMediator and Cache classes
 * Change version string from code to normal text
 * Made it possible to add new extensions in general
 * It's now possible to replace the LastTagExtension with your own
 * Added way to add commonmark extensions
 * Make it possible to prepend to the LastTag element
 * Added staticNotifyToEventGetOutput
 * Add new setting to config.example.ini

## [2024.05.07.5](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.5)
 * HotFix: Don't show .gitlab directory in documents
 * Updated changelog

## [2024.05.07.04](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.04)
 * Breaking change: Changed namespaces
 * Update changelog

## [2024.05.07.3](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.3)
 * Declared strict types on public available PHP scripts
 * declared strict types on interfaces
 * Refine LastTagParser extension
 * Declared strict types
 * Refined Cache class
 * Refined GitDocuments class
 * Refined Helpers classed
 * Refine Package class
 * Require strict types on PluginsMediator
 * Refine things in Route class.
 * Refine things in Theme class.
 * Made imports prettier
 * Further split LastTagParser into multiple parts
 * Simplify things in LastTagParser
 * Remove exec from LastTagParser
 * Added new documents that should help contributers

## [2024.05.07.2](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.2)
 * Fix bug with links and capital letters

## [2024.05.07.1](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07.1)
 * Hot Fix: Fixes issue with fetch and pull

## [2024.05.07](https://gitlab.com/InuitViking/imms/-/releases/2024.05.07)
 * Update GitDocuments class
 * Sort imports
 * Added new URL endpoint for git
 * Added descriptions of new options
 * converted tabs to spaces
 * Converted tabs to spaces within all php/interfaces/* files
 * Converted tabs to spaces within all php/classes/* files
 * Added comments to new extension
 * Added new settings for git
 * Force exit code 0 for bearer
 * Updated LastTagParser
 * Add curl as a dependency
 * Added new CommonMark extension baked in
 * Use spaces instead of tabs
 * Added new custom extension and use spaces
 * Update changelog
 * Update changelog
 * Updating pipeline
 * Adding image with no apache caching
 * Updated changelog
 * updated changelog

## [2024.03.22.3](https://gitlab.com/InuitViking/imms/-/releases/2024.03.22.3)
 * Fixed issue where Theme used native PHP file functions
 * Refactored Theme.php
 * Made it possible to notify plugins of events statically
 * Update changelog

## [2024.03.22.2](https://gitlab.com/InuitViking/imms/-/releases/2024.03.22.2)
 * No hardcorded theme; define theme with cookie
 * Remove unused imports
 * Shift page load event to happen earlier
 * Updated changelog

## [2024.03.22.1](https://gitlab.com/InuitViking/imms/-/releases/2024.03.22.1)
 * Updated script for creating changelog
 * Forgot to add key
 * Fix issue with packages with more than one dash in machine name
 * Remove unused imports
 * Fix #8 - Refactored PluginsMediator
 * Fix 50% of #8
 * Refactor route a tiny bit to do less if the situation calls for it
 * Make use of DI containers in Cache
 * rewrote Route to make use of dependency injection
 * Instead of 'component', use
 * Remove unused imports
 * Refactor Helpers class
 * Remove unused imports
 * Add league/container; we're going to use it later
 * Add some order and comments to the Cache class
 * Enabled xdebug for development
 * Added a link to the wiki
 * reaorder badges
 * Added badges to README

## [2024.03.22.0](https://gitlab.com/InuitViking/imms/-/releases/2024.03.22.0)
 * Fix release notes
 * Update changeog
 * Don't push changelog
 * Still trying to fix pipeline
 * Try to fix the pipeline again...
 * Fix ci git user
 * Try to fix pipeline
 * Fix tokens in pipeline
 * Fix more things in pipeline...
 * Fix pipeline
 * Fix pipeline
 * Fix docker image building
 * Fixed comments on most files
 * Updater docker image to contain zip extensions
 * Sync uploads more often
 * Update default theme to newer version
 * Fix dash or underline on menu and search links
 * Update docker config.ini to match newer versions
 * Potentially fix syncing
 * Added missing AdapterInterface
 * Potentially fix sync method
 * Added unstable tag to docker image
 * Readded syncing of uploads
 * Updated readme to be more informative
 * Fix bearer vulnerabilities
 * Fixed use of shell commands in Package class
 * Fixed shell injection on search
 * Removed unused method
 * Fixed issue on cache clearing and shell injetion
 * Simply routing a tiny bit
 * Fix file modification issue
 * Fixed bug where checksum files were created wrong
 * Typical reading and and writing of files changed
 * Rewrote the git class to use a dependency
 * Attempt to fix shell injection problem
 * Added league/plates and made use of it
 * Updated theme
 * Updated README regarding dependencies
 * Fix CWE-94
 * Update .gitlab-ci.yml file
 * Added generation of release notes
 * Updated changelog generation and pipeline
 * Fixed docker image for custom installations
 * Trying to find another way to generate changelogs
 * Updated pipeline to hopefully make changelogs work git add .

## [2024.02.26.6](https://gitlab.com/InuitViking/imms/-/releases/2024.02.26.6)
 * Fixed composer.json
 * Fixed dockerfile
 * Restore composer.json
 * Fixed bug, where parameters occured in title
 * Removed BaseComponent.php
 * Fix Package class
 * Rely on plugin to print out the correct HTML
 * Added possibility to render markdown without a file
 * Trying to add a way to display pages from plugin
 * WIP: Updated Routing for custom routes from plugins
 * Update README
 * Theme engine update
 * Remove default theme from imms and update packaging
 * Update to packaging
 * Don't print full stacktrace
 * Updated changelog
 * Made the counter in Route public
 * Update Theme class
 * Update Theme class
 * Update composer.json
 * Updated README

## [2024.02.26.0](https://gitlab.com/InuitViking/imms/-/releases/2024.02.26.0)
 * Remove badges from README
 * Fix image width
 * Updated composer.json
 * Simplify development
 * Finalise PluginsMediator
 * Properly fixed Dockerfile
 * Install composer programmatically in dockerfile
 * Install composer programmatically in dockerfile
 * Install composer programmatically in dockerfile
 * Install composer programmatically in dockerfile
 * Added the foundation of plugin management
 * Handle IMMS plugin packages
 * Update README
 * Updated README
 * Added basic theme functionality
 * Add bearer scanner for test
 * Updated .gitlab-ci.yml
 * Fix docker image link

## [2024.02.14.8-rc5](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.8-rc5)
 * Fix link to docker image

## [2024.02.14.8-rc4](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.8-rc4)
 * (hopefully) fix link to docker image

## [2024.02.14.8-rc3](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.8-rc3)
 * Use a different token that will actually work

## [2024.02.14.8-rc2](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.8-rc2)

No unpublished changes


## [2024.02.14.8-rc1](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.8-rc1)
 * Updated gitlab ci to add release job with release notes as well as changelog
 * Use property value instead of repeating the same thing many times
 * Update readme to include more and better information
 * Change the package name
 * Added necessary directories

## [2024.02.14.7](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.7)
 * Added release job to pipeline

## [2024.02.14.6](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.6)
 * Set a default for checking whther git is enabled or not.
 * Fix the issue about user creation in docker

## [2024.02.14.5](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.5)
 * Actually hotfix docker image

## [2024.02.14.4](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.4)
 * Fix whitescreen of death in docker image

## [2024.02.14.3](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.3)
 * Fix pipeline

## [2024.02.14.2](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.2)
 * Make sure git is installed in dockerfile
 * Remove changelog handling in pipeline
 * Updated config.ini
 * Update config.ini comments
 * Remove gitonomy/gitlib from composer
 * Only use git when git is enabled
 * Only use git class when git is enabled + simplify class
 * Don't throw errors when rendering anew from git
 * Added built in git
 * Add dependencies
 * Remove an echo
 * Only run when creating tags
 * Publish docker image on deploy gitlab stage
 * use fallback version to git sha
 * Fix gitlab-ci services format
 * Build docker images via gitlab-ci
 * Did some changes in Cache and Route to allow for use on a CLI.
 * Allow setting your own ini file

## [2024.02.14.1](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.1)
 * Added SFTP support in docker image
 * Cache anew when file has changed or if file is x seconds oldd
 * When caching, create representative checksum file
 * HTML files now also can be cached next to Markdown files
 * updated docker compose example file
 * Updated readme
 * Shift logo to use height in stead of widht, so long logos can be used
 * Remove unused usings
 * Made header fixed, added a 'home' link (logo), Added hack font without CDN
 * Set font for tables
 * Simplified Routing class by A LOT
 * Make sure every setting has a default value, if they're to be omitted from config.ini
 * Properly display a 404 when default_view is missing
 * Use a custom docksal stack for development; no need for db, when db isn't used
 * Choose a CDN for highlight.js
 * Fix some CSS regarding code blocks
 * Remove unused library
 * Remove unused library
 * Remove unused code and rename variable to something that makes sense
 * Make Cache.php more readable
 * Improve readability, accessibility, SEO, and browser load time.
 * Caching improvements:
 * Made sure the comments also explain the type of the value
 * Added comments to config.ini to explain each setting.
 * Rewrite of the config file and the reflected changes.
 * Remove some comments to actually make things more readable
 * Minor optimization of Cache class
 * Minor optimization of Route class
 * Minor optimization of Helper class
 * HTML optimisation
 * Added compression to PHP
 * Added better caching to apache
 * Fix spelling error in apache config
 * Update .gitlab-ci.yml file
 * Update dockerfile with more comments
 * Updateed README
 * Here be config files for docker
 * Upgraded to PHP8.3, added opcache and Apache cache for max speed nyoom
 * Added more tools for debugging
 * Added xdebug to docksal
 * Removed ColourPalette.png
 * Removed ColourPalette.png
 * Added style for blockquote tags
 * Add lazy loading of images
 * Add comments to the new Route functions
 * Update Cache->createDirs to allow null in  argument
 * Further simply Cache.php for easier maintenance.
 * Restored config.ini
 * Rewrote most classes to make them simpler and more easy to read
 * Minor changes to rendering: headers and HTML
 * Improve the display of code and pre tags
 * Update README.md
 * Keep the errors directory within the cache directory
 * Update .gitignore
 * Split error page config in two
 * Configuration to set which error pages to show
 * Fix title bug
 * Bug fixes
 * Make it possible to use wikilink
 * Make it possible to change internal port with env variable APACHE_PORT
 * Support subpages without duplicate menu entries
 * Actually make TOC GitLab compatible
 * Added support for GitLab style TOC
 * Update Dockerfile and docker-compose.yml with the removal of cron
 * Update README to be a bit more user friendly
 * Removed .idea folder
 * Updated gitignore
 * Removed cache cronjob as it is unnecessary now
 * Automatic update of cache without cronjobs
 * Removed unnecessary comment
 * Added cache eol in config
 * Change workdir in dockerfile
 * updated .idea
 * Updated readme with the new changes
 * updated docker compose file with new container registry
 * Added docker stuff
 * updated .idea
 * Created a new directory for uploads
 * Fix bug where images weren't loaded when entering subpages
 * Fix bug where upload folder isn't properly rsynced
 * Update dependencies
 * Update .idea
 * Copy uploads to proper directory and ignore all non-md files
 * Made use of new dependency
 * Added config path for uploads
 * Added new dependency
 * Updated .idea
 * Split routing method into multiple methods, to simplify maintenance.

## [2024.02.14.0](https://gitlab.com/InuitViking/imms/-/releases/2024.02.14.0)

No unpublished changes


## [2023.01.14.0](https://gitlab.com/InuitViking/imms/-/releases/2023.01.14.0)
 * script and README updated
 * Removed 'Generating a salt' from the README. It is not best practice
 * Updated README. We don't need to generate a cache.
 * Updated script and README
 * Updated .idea stuff
 * Added scripts submodule
 * Deleted scripts
 * Updated README
 * Fixed typo in README
 * Deleted ivmdcli.php
 * Update README
 * Only compile SCSS when new MD pages are being rendered.
 * Update README
 * Removed OpenSans
 * Cleaned up CSS variables
 * Started cleanup of CSS
 * Removed composer lock
 * Better formatted readme and added new section
 * removed documents
 * ignoring documents going forward
 * Added markers
 * Added emojis
 * Added a bit of styling around the iframe for consistency in looks.
 * Added youtube iframs
 * Updated composer
 * Removed unnecesarry library
 * Added task list as example
 * Added footnotes
 * WIP: Added footnotes
 * Changed font of footer.
 * Added more list examples and fixed font for lists.
 * Finalised table styling
 * Added external links extension
 * removed all traces of embed
 * Removed embed
 * Upgraded composer packages
 * Added embed/embed
 * Added styling for tables.
 * Added a border around table
 * Added more coloured hints and a default colour.
 * Added examples to showcase more hints and added a table example
 * Fixed general styling of hints and code, while adding colours for two hint types
 * Added more hint examples and fixed typo
 * Update Readme
 * Added some styling for the hints; WIP
 * Added hints (unstyled)
 * Removed highlight.js and added highlighter for commonmark
 * Added commonmark-highlighter
 * Added github flavoured markdown and heading permalinks
 * Added comments and replaced underscores with spaces in dir names also
 * Moved spaces file to new spaces dir
 * Change docksal settings to use specific images
 * Update composer packages

## [2022.04.12.6](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.6)
 * Added link to project in footer
 * Made the sidebar prettier

## [2022.04.12.5](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.5)
 * Updated gitignore
 * Updated gitignore
 * Updated gitignore
 * Updated .gitignore
 * Properly added alternating colours
 * Updated .gitignore
 * Updated composer packages
 * Add return types to methods
 * Update CLI image
 * Removed CSS that caused weird background colouring.

## [2022.04.12.4](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.4)
 * Fixed CSS, config file, and title
 * Upgraded packages
 * Fixed search result bug
 * Fixed up some minor HTML and added a title method
 * simplified things in Route.php
 * clean css
 * idk

## [2022.04.12.3](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.3)
 * Fix README.md
 * Fixed hljs bug
 * Fixed underscore in name
 * Enable symfony for ide
 * AAA
 * Added a bunch of new styling
 * Attempted to update favicon, fixed sidebar to look better, updated md files
 * Added a favicon and moved relevant files
 * Updated packages and changed name to IMMS
 * Made the searchbar more user friendly
 * Added logo with gimp file
 * Add CHANGELOG
 * Add LICENSE

## [2022.04.12.2](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.2)
 * Made caching use bash instead, as that is the faster better solution right now.
 * Patched caching so caching works with the correct permissions
 * Patched search engine so it actually works outside of docker

## [2022.04.12.1](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12.1)
 * Patched up caching and routing errors, while fixing the sidebar when using long text
 * Added some notes for myself

## [2022.04.12](https://gitlab.com/InuitViking/imms/-/releases/2022.04.12)
 * Forgot to add one more thing
 * Fixed up readme and index
 * Added highlight.js
 * Added fonts and more styling, also more markups.
 * I changed so many things; but it looks almost done.
 * Fixed some markup, added some scss
 * Added static function to create menu.
 * Added PHPDocs.
 * Added better rootdirectory figuring outing.
 * Added better rootdirectory figuring outing.
 * Added ini config file... doubting if the use of getcwd will work outside of docksal.
 * Added more cli functionality
 * Added CLI script and edited Cache
 * Created some cache clearing and some other cleanup
 * Managed to create MD→HTML caching. Next should be about checking if the cache is up to date.
 * Testing out caching
 * Tried some scssphp stuff
 * Upgraded docksal PHP CLI to 8.1, and fixed some things in Route class
 * I have added code to the routing function and I have added a lot of documents to test with. Shit seems to work. :D
 * Added a route class (no code) and made some more directories
 * Added docksal stuff
 * Initial commit
