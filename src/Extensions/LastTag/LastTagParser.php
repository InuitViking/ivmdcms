<?php

declare(strict_types=1);

namespace Imms\Extensions\LastTag;

use Imms\Classes\Bootstrapper;
use CzProject\GitPhp\GitException;
use CzProject\GitPhp\Runners\CliRunner;
use Imms\Classes\PluginsMediator;
use League\CommonMark\Extension\CommonMark\Node\Inline\Code;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;
use League\CommonMark\Node\Block\AbstractBlock;
use League\CommonMark\Node\Inline\Text;
use League\CommonMark\Parser\Cursor;
use League\CommonMark\Parser\Inline\InlineParserInterface;
use League\CommonMark\Parser\Inline\InlineParserMatch;
use League\CommonMark\Parser\InlineParserContext;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;

class LastTagParser implements InlineParserInterface {

    private static string $urlRegex = '(?:http[s]?:\/\/.)?(?:www\.)?[-a-zA-Z0-9@%._+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_+.~#?&\/=]*';

    /**
     * Gets the match definition of an image link that looks something like this:
     *
     * - `![latest tag](https://gitlab.com/inuitviking/imms)`
     * - `![latest release](https://gitlab.com/inuitviking/imms/-/badges/release.svg?order_by=release_at&value_width=110)`
     *
     * @return InlineParserMatch
     */
    public function getMatchDefinition(): InlineParserMatch {
        return InlineParserMatch::join(
            InlineParserMatch::string('!'),
            InlineParserMatch::regex('\[(latest|last) (tag|release)\]'),
            InlineParserMatch::string('('),
            InlineParserMatch::regex(self::$urlRegex),
            InlineParserMatch::string(')'),
        );
    }

    /**
     * Parses the matched text.
     *
     * If everything below matches, replace the text with the latest version found in the remote git repository
     *
     * @param InlineParserContext $inlineContext
     * @return bool
     */
    public function parse(InlineParserContext $inlineContext): bool {
        $cursor = $inlineContext->getCursor();
        $fullMatch = strtolower($inlineContext->getFullMatch());
        return $this->elementToLastTag($cursor, $inlineContext, $fullMatch);
    }

    /**
     * Gets the last release/tag from the match.
     * Returns `false` if the version wasn't found.
     *
     * @param string $match
     * @return string|false
     */
    private function getLatestRelease (string $match): string|false {
        // Remove all the unnecessary fluff from the match
        $replacements = [
            '![latest tag](',
            '![latest release](',
            '![last tag](',
            '![last release](',
            ')'
        ];
        $match = str_replace($replacements, '', $match);
        $match = preg_replace('/\/-.*/', '', $match);
        $fullMatch = $match;

        $root = Bootstrapper::rootDirectory();
        $adapter = new LocalFilesystemAdapter($root);
        $filesystem = new Filesystem($adapter);

        try {
            if ($filesystem->fileExists("tmp/$fullMatch") && (time() - $filesystem->lastModified("tmp/$fullMatch") < 7200)) {
                return $filesystem->read("tmp/$fullMatch");
            } else {
                return $this->fetchActualLatestRelease($match, $fullMatch, $filesystem);
            }
        } catch (FilesystemException) {
            echo "Couldn't store the latest release - Check file permissions!";
            die(99);
        }
    }

    /**
     * Fetches the actual latest release from git.
     *
     * @param string $match
     * @param string $fullMatch
     * @param Filesystem $filesystem
     * @return string|false
     */
    private function fetchActualLatestRelease (string $match, string $fullMatch, Filesystem $filesystem): string|false {
        // Make use of the CliRunner within GitPhp
        $runner = new CliRunner();
        $cwd = Bootstrapper::rootDirectory();

        // If the remote address isn't reachable, we'll try with tokens!
        $match = $this->nonReachableRemoteMatch($match);

        if (!str_ends_with($match, '.git')) {
            $match = $match . '.git';
        }

        $gitArgs = ['ls-remote', '--tags', '--sort=v:refname',$match];

        try {
            // Run the command and get the output as an array (and then sanitize it)
            $output = $this->sanitizeOutput($runner->run($cwd, $gitArgs)->getOutput());
            $output = $this->removeFromArrayIfContains("-rc", $output);
            $output = $this->removeFromArrayIfContains("-RC", $output);
            $lastTag = end($output);

            if (!$filesystem->fileExists('tmp')) {
                $filesystem->createDirectory('tmp');
            }

            $filesystem->write("tmp/$fullMatch", $lastTag);

            // Return the last output
            return $lastTag;
        } catch (GitException) {
            echo "Failed to fetch latest tag";
        } catch (FilesystemException) {
            echo "Failed to write in tmp directory - Check file permissions!";
        }

        return false;
    }

    /**
     * Checks whether an array contains a substring, and if it does, remove that particular index.
     *
     * @param string $subString
     * @param array $array
     * @return array
     */
    private function removeFromArrayIfContains (string $subString, array $array): array {
        foreach($array as $key => $value) {
            if (str_contains($value, $subString)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * Checks whether the provided url has an HTTP code greater than or equal to 200 and lower than 300.
     *
     * @param string $match
     * @return bool
     */
    private function checkRemoteReachable (string $match): bool {
        $remoteRepo = curl_init($match);
        @curl_setopt($remoteRepo, CURLOPT_HEADER  , true);
        @curl_setopt($remoteRepo, CURLOPT_NOBODY  , true);
        @curl_setopt($remoteRepo, CURLOPT_RETURNTRANSFER , true);
        @curl_setopt($remoteRepo, CURLOPT_TIMEOUT, 20);
        curl_exec($remoteRepo);
        $httpCode = curl_getinfo($remoteRepo, CURLINFO_HTTP_CODE);
        curl_close($remoteRepo);

        if ($httpCode >= 200 && $httpCode < 300) {
            return true;
        }

        return false;
    }

    /**
     * If the remote is not reachable, we'll change the $match to use tokens.
     * If it is reachable, we'll not change anything, and simply return the $match in its original state.
     *
     * @param string $match
     * @return string
     */
    private function nonReachableRemoteMatch (string $match): string {
        if (!$this->checkRemoteReachable($match)) {
            $config = Bootstrapper::getIni();
            $tokenUser = $config['git']['token_name'];
            $tokenPass = $config['git']['token_pass'];

            $authString = "$tokenUser:$tokenPass@";

            $url = parse_url($match);

            $matchReplacements = [
                $url['scheme'],
                '://'
            ];

            $match = str_replace($matchReplacements, '', $match);
            return $url['scheme'] . '://' . $authString . $match;
        }
        return $match;
    }

    /**
     * Is meant to fix the output array of the CliRunner.
     *
     * Strips ref strings from tag numbers, removes unwanted tags, and lastly sorts it
     *
     * @param array $output
     * @return array
     */
    private function sanitizeOutput (array $output): array {
        foreach ($output as &$item) {
            $item = strrchr($item, '/');
            $item = str_replace('/', '', $item);
        }
        $output = array_filter($output, function ($var) { return stripos($var, '^{}') === false; });
        $output = array_filter($output, function ($var) {return preg_match('/^(((v\d)|(\d))+.)/', $var); });
        sort($output, SORT_NATURAL);

        return $output;
    }

    /**
     * Converts the matching element to the tag we want it to be.
     *
     * It returns true or false depending on whether it succeeds.
     *
     * @param Cursor $cursor
     * @param InlineParserContext $inlineContext
     * @param string $fullMatch
     * @return bool|void
     */
    private function elementToLastTag (Cursor $cursor, InlineParserContext $inlineContext, string $fullMatch) {
        // Save the cursor state in case we need to rewind and bail
        $previousState = $cursor->saveState();

        // The symbol must not have any other characters immediately prior
        $previousChar = $cursor->peek(-1);
        if ($previousChar !== null && $previousChar !== ' ' && $previousChar !== '[') {
            // peek() doesn't modify the cursor, so no need to restore state first
            return false;
        }

        // Advance past the symbol to keep parsing simpler
        $cursor->advance();

        // Parse the match value
        $identifier = $cursor->match("^\[(latest|last) (tag|release)]\((?:http[s]?://.)?(?:www\.)?[-a-zA-Z0-9@%._+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_+.~#?&/=]*\)^i");
        if ($identifier === null) {
            // Regex failed to match; this isn't a valid link
            $cursor->restoreState($previousState);
            return false;
        }

        $identifier = strtolower(substr($identifier, 0, -1));

        if (str_contains($identifier, 'latest tag') || str_contains($identifier, 'latest release')
            || str_contains($identifier, 'last tag') || str_contains($identifier, 'last release')) {
            $container = $inlineContext->getContainer();
            $container->appendChild(new Text($this->getLatestRelease($fullMatch)));

            // Gets output from plugins via the `latest_tag` event, and passes an array to those plugins that might
            // find it useful
            $eventOutput = PluginsMediator::staticNotifyToEventGetOutput('latest_tag', ['fullMatch' => $fullMatch]);
            if (!empty($eventOutput)) {
                // If it's not empty, go through the output
                $this->getEventOutput($container, $eventOutput);
            }

            return true;
        }
    }

    /**
     * Returns the output from events (via plugins) and prepends to the LastTag link.
     *
     * @param AbstractBlock $container
     * @param array $eventOutput
     * @return void
     */
    private function getEventOutput (AbstractBlock $container, array $eventOutput): void {
        foreach ($eventOutput as $output) {
            // If the output is empty, don't go further
            if (empty($output)) {
                continue;
            }

            // Prepend a pipe as a separator
            $container->prependChild(new Text(" | "));

            // If the output is a string, simply prepend it as text
            if (is_string($output)) {
                $container->prependChild(new Text($output));
            }

            // If it's an array, check what type of output the plugin wants to add
            if (is_array($output)) {
                if ($output['type'] === 'link') {
                    // If it's a link, add the necessary parameters to the Link object.
                    $container->prependChild(new Link($output['link'], $output['label'], $output['title']));
                } elseif ($output['type'] === 'code') {
                    // If it's code, add the necessary parameters to the Code object.
                    $container->prependChild(new Code($output['code']));
                } elseif ($output['type'] === 'text') {
                    // If it's text, add the necessary parameters to the Text object.
                    $container->prependChild(new Text($output['text']));
                }
            }
        }
    }
}
