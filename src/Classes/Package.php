<?php

declare(strict_types=1);

namespace Imms\Classes;

use Composer\InstalledVersions;
use League\Flysystem\Filesystem;
use Composer\Installer\PackageEvent;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;

class Package {

    /**
     * A private string constant containing the 'plugins' directory.
     */
    private const string PLUGINS_FROM_ROOT = '/src/plugins/';

    /**
     * A private string constant containing the 'themes' directory
     */
    private const string THEMES_FROM_ROOT = '/src/themes/';

    /**
     * A private string constant containing the package type for plugins for IMMS in composer.
     */
    private const string PLUGIN_TYPE = 'imms-plugin';

    /**
     * A private string constant containing the package type for themes for IMMS in composer.
     */
    private const string THEME_TYPE = 'imms-theme';

    /**
     * If a package is of the type imms-plugin or imms-theme, sync it to the plugins or theme directory.
     *
     * @param PackageEvent $event
     *
     * @return void
     */
    public static function postPackageInstall (PackageEvent $event): void {
        // Get which package was installed
        $package = $event->getOperation()->getPackage();

        // If the type is an imms-theme or plugin...
        if ($package->getType() === self::PLUGIN_TYPE || $package->getType() === self::THEME_TYPE) {
            // Get the installation manager
            $installationManager = $event->getComposer()->getInstallationManager();

            // Get the installation path
            $originDir = $installationManager->getInstallPath($package);

            // If it exists and is a directory
            if (file_exists($originDir) && is_dir($originDir)) {
                // Define the package or theme directory (See the if statement)
                $packageDir = Bootstrapper::rootDirectory() . self::PLUGINS_FROM_ROOT . $package->getName();
                if ($package->getType() === self::THEME_TYPE) {
                    $packageDir = Bootstrapper::rootDirectory() . self::THEMES_FROM_ROOT . $package->getName();
                }
                // Create the directory recursively
                @mkdir($packageDir, recursive: true);
                // Copy the installation path to the package directory.
                Helpers::sync("$originDir/", $packageDir);
            }
        }
    }

    /**
     * If a package is of the imms-plugin or imms-theme and has been updated, update it within src/plugins.
     *
     * @return void
     */
    public static function postUpdate (): void {

        // Create an array containing all imms-plugins and imms-themes
        $packages[self::PLUGIN_TYPE] = InstalledVersions::getInstalledPackagesByType(self::PLUGIN_TYPE);
        $packages[self::PLUGIN_TYPE] = array_values(array_unique($packages[self::PLUGIN_TYPE]));
        $packages[self::THEME_TYPE] = InstalledVersions::getInstalledPackagesByType(self::THEME_TYPE);
        $packages[self::THEME_TYPE] = array_values(array_unique($packages[self::THEME_TYPE]));

        // Define the plugin and theme root
        $pluginDirRoot = Bootstrapper::rootDirectory() . self::PLUGINS_FROM_ROOT;
        $themeDirRoot = Bootstrapper::rootDirectory() . self::THEMES_FROM_ROOT;

        // Loop through the package array
        foreach ($packages as $type => $package) {
            // Don't do anything if $package is empty
            if ($package === []) {
                continue;
            }
            // Loop through the package data
            foreach ($package as $p) {
                // Get the package directory
                $packageDir = "$pluginDirRoot$p";
                if ($type === self::THEME_TYPE) {
                    $packageDir = "$themeDirRoot$p";
                }
                // Get the installation path
                $installPath = InstalledVersions::getInstallPath($p);
                // Copy the installation path to the package directory.
                Helpers::sync($installPath, $packageDir);
            }
        }
    }

    /**
     * If a package of imms-plugin or imms-theme gets uninstalled, remove it from src/plguins or src/themes
     *
     * @param PackageEvent $event
     *
     * @return void
     */
    public static function postPackageUninstall (PackageEvent $event): void {
        $package = $event->getOperation()->getPackage();

        if ($package->getType() === self::PLUGIN_TYPE || $package->getType() === self::THEME_TYPE) {
            // We're more likely to be uninstalling plugins, so use that as default
            $packageDir = Bootstrapper::rootDirectory() . self::PLUGINS_FROM_ROOT . $package->getName();

            // If it is a theme, then of course change the directory to that.
            if ($package->getType() === self::THEME_TYPE) {
                $packageDir = Bootstrapper::rootDirectory() . self::THEMES_FROM_ROOT . $package->getName();
            }

            // Uninstall the package
            self::uninstallPackage($packageDir);
        }
    }

    /**
     * Removes package from specified package directory; usually imms-plugin or imms-theme.
     *
     * @param string $packageDir
     *
     * @return void
     */
    private static function uninstallPackage (string $packageDir): void {
        $vendorDir = dirname($packageDir, 1);
        self::rmDir($packageDir);
        if (self::isDirEmpty($vendorDir)) {
            self::rmDir($vendorDir);
        }
    }

    /**
     * Check if a directory is empty recursively.
     *
     * @param string $dir
     *
     * @return bool
     */
    private static function isDirEmpty (string $dir): bool {
        $handle = opendir($dir);
        while (false !== ($entry = readdir($handle))) {
            if ($entry !== '.' && $entry !== '..') {
                closedir($handle);
                return false;
            }
        }
        closedir($handle);
        return true;
    }

    /**
     * Recursively removes a directory
     *
     * @param string $dir
     *
     * @return void
     */
    private static function rmDir (string $dir): void {
        $rootDir = Bootstrapper::rootDirectory();
        $adapter = new LocalFilesystemAdapter($rootDir);
        $filesystem = new Filesystem($adapter);
        $dir = str_replace($rootDir, '', $dir);

        try {
            $filesystem->deleteDirectory($dir);
        } catch (FilesystemException) {
            echo "Error occurred when trying to delete directory $dir!";
        }
    }
}
