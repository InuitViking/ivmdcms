<?php

declare(strict_types=1);

namespace Imms\Classes;

use Override;
use Composer\InstalledVersions;
use League\Container\Container;
use Imms\Interfaces\Mediator;

class PluginsMediator implements Mediator {
    private static mixed $staticEvents;
    private static array $staticPlugins;

    /**
     * A private array containing all plugins
     *
     * @var array
     */
    private array $plugins;

    /**
     * Private string containing the root directory
     *
     * @var string
     */
    private string $rootDir;

    /**
     * A private array containing all events
     *
     * @var array
     */
    private array $events;

    // Container object for DI containers
    private Container $container;

    /**
     * PluginsMediator takes care of all IMMS plugins installed through composer.
     *
     * Once a new instance of PluginsMediator is created, it loads all plugins.
     */
    public function __construct (Container $container) {
        $this->plugins = [];
        self::$staticPlugins = [];
        $this->rootDir = Bootstrapper::rootDirectory();
        $this->events = [];
        self::$staticEvents = [];
        $this->container = $container;
        $this->loadPlugins();
    }

    /**
     * Adds a plugin to the list of plugins.
     *
     * The plugin MUST be an instantiated object.
     *
     * @param object $plugin
     * @param string $pluginName
     *
     * @return void
     */
    public function addPlugin (object $plugin, string $pluginName): void {
        $this->plugins[$pluginName] = $plugin;
        self::$staticPlugins[$pluginName] = $plugin;
    }

    /**
     * Loads all plugins and subscribe them to their specified events.
     *
     * All plugins are instantiated and their instantiated object is stored in an array.
     *
     * @return void
     */
    private function loadPlugins (): void {
        // Get a list of all IMMS plugins
        $plugins = InstalledVersions::getInstalledPackagesByType('imms-plugin');
        // Sort through them, because above duplicates them
        $plugins = array_values(array_unique($plugins));
        // Loop through all plugins
        foreach ($plugins as $plugin) {
            $this->instantiateAndSavePlugin($plugin);
        }
    }

    /**
     * Creates an instance of a plugin, and saves it in an array.
     *
     * If the plugins subscribe to certain events, this too will be saved.
     *
     * @param string $plugin
     *
     * @return void
     */
    private function instantiateAndSavePlugin(string $plugin): void {
        // Split plugin name based on /
        $pluginName = explode('/', $plugin);
        $pluginNamespace = explode('-', $pluginName[1]);
        foreach ($pluginNamespace as &$pns) {
            $pns = ucfirst($pns);
        }
        $pluginName[1] = implode('', $pluginNamespace);

        // Require the plugin (should probably have an autoloader for this?)
        require_once $this->rootDir.'/src/plugins/'.$plugin.'/src/'.$pluginName[1].'.php';

        // Create the namespace of the plugin
        $plugin = '\\'.$pluginName[0].'\\'.$pluginName[1].'\\'.$pluginName[1];

        // Add the plugin to the list
        $object = $this->container->get($plugin);
        $this->addPlugin($object, $pluginName[1]);
        $this->addPluginToAllSubscribedEvents($object, $pluginName[1]);
    }

    /**
     * Adds plugin to all subscribed events
     *
     * $plugin must be an instance of the "main class" of the plugin.
     *
     * @param object $plugin
     * @param string $pluginName
     *
     * @return void
     */
    private function addPluginToAllSubscribedEvents (object $plugin, string $pluginName): void {
        if (isset($plugin->subscribedEvents)) {
            foreach ($this->plugins[$pluginName]->subscribedEvents as $subscribedEvent) {
                $this->subscribePluginToEvent($pluginName, $subscribedEvent);
            }
        }
    }

    /**
     * Get an array of all instantiated plugins.
     *
     * @return array
     */
    public function getPlugins (): array {
        return $this->plugins;
    }

    /**
     * Subscribe a specified plugin to a specified event.
     *
     * @param string $plugin
     * @param string $event
     *
     * @return void
     */
    public function subscribePluginToEvent (string $plugin, string $event): void {
        if (!isset($this->events[$event])) {
            $this->events[$event] = [];
            self::$staticEvents[$event] = [];
        }
        $this->events[$event][] = $plugin;
        self::$staticEvents[$event][] = $plugin;
    }

    /**
     * Send a notification to all plugins subscribed to a specified event.
     *
     * @param string $event
     *
     * @return void
     */
    #[Override] public function notifyToEvent (string $event): void {
        // Get all events
        $events = array_keys($this->events);
        if (in_array($event, $events)) {
            // If $even exists in $events, notify all the plugins subscribed to this event.
            foreach ($this->events[$event] as $plugin) {
                $this->notify($plugin, $event);
            }
        }
    }

    /**
     * Does exactly the same as notifyToEvent, but allows for static calling of this function, so we don't need
     * to have a DI container to call this function. The plugin must have the `getNotification(string $event)` method.
     *
     * @param string $event
     * @return void
     */
    public static function staticNotifyToEvent (string $event): void {
        // Get all events
        $events = array_keys(self::$staticEvents);
        if (in_array($event, $events)) {
            // If $even exists in $events, notify all the plugins subscribed to this event.
            foreach (self::$staticEvents[$event] as $plugin) {
                // Check if plugin is subscribed to $event
                if (in_array($plugin, self::$staticEvents[$event])) {
                    // If yes, call the getNotification method in the instantiated plugin object.
                    self::$staticPlugins[$plugin]->getNotification($event);
                }
            }
        }
    }


    /**
     * Does exactly the same as staticNotifyToEvent, but returns the output of the function, instead of just running the
     * plugin function. This is for when you need IMMS to handle the output, instead of the plugin. This can be beneficial
     * when there is some text that needs to be changed or edited from within IMMS, instead of add to the general IMMS
     * experience. The plugin must have the `getNotificationWithOutput(string $event)` method.
     *
     * @param string $event
     * @param array|null $parameters
     * @return array
     */
    public static function staticNotifyToEventGetOutput (string $event, array $parameters = null): array {
        $events = array_keys(self::$staticEvents);
        $pluginOutput = [];
        if (in_array($event, $events)) {
            foreach (self::$staticEvents[$event] as $plugin) {
                if (in_array($plugin, self::$staticEvents[$event])) {
                    $pluginOutput[] = self::$staticPlugins[$plugin]->getNotificationWithOutput($event, $parameters);
                }
            }
        }
        return $pluginOutput;
    }

    /**
     * This function is almost the same as `staticNotifyToEventGetOutput`, but is meant specifically for League\Commonmark
     * extensions.
     *
     * Returns an array of (hopefully) CommonMark extensions.
     *
     * @param string $event
     * @return array
     */
    public static function staticNotifyCommonMarkExtensions (string $event): array {
        $events = array_keys(self::$staticEvents);
        $pluginExtensions = [];
        if (in_array($event, $events)) {
            foreach (self::$staticEvents[$event] as $plugin) {
                if (in_array($plugin, self::$staticEvents[$event])) {
                    $pluginExtensions[] = self::$staticPlugins[$plugin]->getCommonMarkExtension($event);
                }
            }
        }
        return $pluginExtensions;
    }

    /**
     * This function is almost the same as `staticNotifyCommonMarkExtensions`, but is meant specifically for League\Commonmark
     * DocumentParsed Event listeners.
     *
     * Returns an array of (hopefully) CommonMark extensions.
     *
     * @param string $event
     * @return array
     */
    public static function staticNotifyCommonMarkEventListeners (string $event): array {
        $events = array_keys(self::$staticEvents);
        $pluginExtensions = [];
        if (in_array($event, $events)) {
            foreach (self::$staticEvents[$event] as $plugin) {
                if (in_array($plugin, self::$staticEvents[$event])) {
                    $pluginExtensions[] = self::$staticPlugins[$plugin]->getCommonMarkEventListener($event);
                }
            }
        }
        return $pluginExtensions;
    }

    /**
     * Notifies a specified plugin that a specified event has occurred.
     *
     * @param string $pluginName
     * @param string $event
     *
     * @return void
     */
    #[Override] public function notify (string $pluginName, string $event): void {
        // Check if plugin is subscribed to $event
        if (in_array($pluginName, $this->events[$event])) {
            // If yes, call the getNotification method in the instantiated plugin object.
            $this->plugins[$pluginName]->getNotification($event);
        }
    }
}
