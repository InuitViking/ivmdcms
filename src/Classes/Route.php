<?php

declare(strict_types=1);

namespace Imms\Classes;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ScssPhp\ScssPhp\Exception\SassException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\CommonMark\Exception\CommonMarkException;

require_once '../vendor/autoload.php';

class Route {
    /**
     * Private string containing the current path the user is on.
     *
     * @var string|mixed
     */
    private string $path;

    /**
     * Whether a Markdown document exists of the current path
     *
     * @var bool
     */
    private bool $mdExists;

    /**
     * Whether an HTML document exists of the current path
     *
     * @var bool
     */
    private bool $htmlExists;

    /**
     * Whether a cached version of the current path exists.
     *
     * @var bool
     */
    private bool $cacheExists;

    /**
     * A private array containing the IMMS configuration
     *
     * @var array|false
     */
    private array|false $config;

    /**
     * Private static string containing the html extension; just to make sonarcube shut up.
     *
     * @var string
     */
    private static string $htmlExt = '.html';

    /**
     * Private static string containing the markdown extension; just to make sonarcube shut up.
     *
     * @var string
     */
    private static string $mdExt = '.md';

    /**
     * Private string containing the root directory
     *
     * @var string
     */
    private string $rootDir;

    /**
     * Private string containing the path to all markdown
     *
     * @var string|mixed
     */
    private string $mdPath;

    /**
     * Private string containing the path to the cache
     *
     * @var string|mixed
     */
    private string $cachePath;

    /**
     * Private integer containing the lifespan of a cached file.
     *
     * @var int|mixed
     */
    private int $cacheEOL;

    /**
     * Private PluginsMediator for getting the plugins.
     *
     * @var PluginsMediator
     */
    private PluginsMediator $pluginMediator;

    /**
     * Private Filesystem for handling filesystem operations.
     *
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * Private Cache for handling cache operations.
     *
     * @var Cache
     */
    private Cache $cache;

    /**
     * Private Theme for handling theme operations.
     *
     * @var Theme
     */
    private Theme $theme;
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * Handles routing in general.
     *
     * Will always load rendered markdown/stitched-together HTML files - see Cache.php
     *
     * If there is no available HTML file, it will check if the MD/HTML exists, and render it if it does.
     *
     * If not, it will throw a 404.
     *
     * @param PluginsMediator|null $pluginsMediator
     */
    public function __construct(Cache $cache, Theme $theme, PluginsMediator $pluginsMediator = null) {
        // Set all the private properties
        $this->config = Bootstrapper::getIni();
        $this->mdPath = $this->config['app']['md_path'] ?? '/src/documents/';
        $this->cachePath = $this->config['app']['html_path'] ?? '/web/assets/cache/';
        $this->cacheEOL = $this->config['app']['cache_eol'] ?? 900;
        $this->pluginMediator = $pluginsMediator;
        $adapter = new LocalFilesystemAdapter(Bootstrapper::rootDirectory());
        $this->filesystem = new Filesystem($adapter, ['checksum_algo' => 'sha256']);
        $this->rootDir = Bootstrapper::rootDirectory();
        $this->path = ltrim(urldecode($_SERVER['REQUEST_URI']),'/');
        $this->cache = $cache;
        $this->theme = $theme;

        $this->log = new Logger('Cache');
        $this->log->pushHandler(new StreamHandler($this->config['app']['log_path'], Logger::WARNING));

        // If path has "?" remove it from the path, as it will be read wrong.
        if (str_contains($this->path, '?')) {
            $this->path = substr($this->path, 0, strpos($this->path, '?'));
        }

        // If path shows up empty, use the default view
        if ($this->path === '') {
            $this->path = $this->config['app']['default_view'] ?? 'index';
        }

        // If git is enabled, clone (if it isn't already.)
        if (GitDocuments::gitEnabled()) {
            $git = new GitDocuments();
            $git->clone();
        }

        // Check if Markdown/HTML documents and cached files exist for this current path.
        try {
            $this->mdExists = $this->filesystem->fileExists($this->mdPath . $this->path . self::$mdExt);
            $this->htmlExists = $this->filesystem->fileExists($this->mdPath . $this->path . self::$htmlExt);
            $this->cacheExists = $this->filesystem->fileExists($this->cachePath . $this->path . self::$htmlExt);
        } catch (FilesystemException $e) {
            $this->log->error($e->getMessage());
            $this->log->error($e->getTraceAsString());
        }
    }

    /**
     * Displays the page, and caches it if it isn't cached already.
     */
    public function displayPage (): void {
        // If the source file exists for the current path
        if ($this->mdExists || $this->htmlExists) {
            // Get path to cached HTML file as well as the markdown file
            $cacheFile = $this->cachePath . $this->path . self::$htmlExt;

            // Figure out the correct extension
            $extension = self::$mdExt;
            if ($this->htmlExists) {
                $extension = self::$htmlExt;
            }
            // Define source file
            $sourceFile = $this->mdPath . $this->path . $extension;
            $gitUploadPath = $this->rootDir.'/'.$this->mdPath.'uploads';
            $uploadPath = $this->rootDir.'/'.$this->config['app']['upload_path'];

            try {
                // Check if cache exists, if no, create it
                if (!$this->cacheExists) {
                    $this->cache->md2html($sourceFile);
                    Helpers::sync($gitUploadPath, $uploadPath);
                }

                // If cache is too old or the source file has changed, update cache
                $cacheFileAge = time() - $this->filesystem->lastModified($cacheFile) > $this->cacheEOL;
                if ($this->fileChanged($sourceFile) || $cacheFileAge) {
                    $this->cache->md2html($sourceFile);
                    Helpers::sync($gitUploadPath, $uploadPath);
                }

                // Show the cached file
                echo $this->filesystem->read($cacheFile);
            } catch (FilesystemException | CommonMarkException | SassException $e) {
                $this->log->error($e->getMessage());
                $this->log->error($e->getTraceAsString());
            }
            return;
        }

        // Fetch and pull from git, and delete the tmp directory.
        if ($this->path === 'git_update' && $this->config['git']['enabled']) {
            $this->pluginMediator->notifyToEvent('git_update');
            $git = new GitDocuments();
            $git->fetchPull();
            $this->filesystem->deleteDirectory('tmp');
            header('Location: /');
            return;
        }

        // If a plugin subscribes to this path load what the plugin has for it.
        if ($this->isPathInPlugin()) {
            $templates = $this->theme->themeObject->getEngine();
            echo $templates->render('partials/header');
            $this->getFunctionFromPlugin($this->path);
            echo $templates->render('partials/footer');
            return;
        }

        // Show 404
        $this->show404();
    }

    /**
     * Shows the error page
     *
     * @return void
     */
    private function show404 (): void {
        // Set the response code to 404
        http_response_code(404);

        $errorHTMLPath = $this->config['app']['error_html_path'] ?? 'web/assets/cache/errors/';
        $errorMDPath = $this->config['app']['error_md_path'] ?? 'src/errors/';

        try {
            // Create cache if the HTML file exists (it should)
            if (!$this->filesystem->fileExists($errorHTMLPath . '404' . self::$htmlExt)) {
                $this->cache->md2html($errorMDPath . '404' . self::$mdExt, ['error_page' => true]);
            }

            // Show 404
            echo $this->filesystem->read($errorHTMLPath . '404' . self::$htmlExt);
        } catch (FilesystemException | CommonMarkException | SassException $e) {
            $this->log->error($e->getMessage());
            $this->log->error($e->getTraceAsString());
        }
    }

    /**
     * Check if the path is within one of the plugins
     *
     * @param $path
     *
     * @return bool
     */
    public function isPathInPlugin($path = null): bool {
        if ($path === null) {
            $path = $this->path;
        }

        $path = "/$path";

        foreach ($this->pluginMediator->getPlugins() as $plugin) {
            if (in_array($path, $plugin->subscribedPaths)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the method/function from a plugin
     *
     * @param $path
     *
     * @return void
     */
    public function getFunctionFromPlugin ($path = null): void {
        if ($path === null) {
            $path = $this->path;
        }
        $path = "/$path";

        foreach ($this->pluginMediator->getPlugins() as $plugin) {
            if (in_array($path, $plugin->subscribedPaths)) {
                $plugin->getPathNotification($path);
            }
        }
    }

    /**
     * Checks whether a file has changed; returns true if yes, and false if not.
     *
     * @param string $file
     *
     * @return bool
     *
     * @throws FilesystemException
     */
    public function fileChanged (string $file): bool {
        // Define the checksum directory
        $checksumDir = 'src/checksums/';
        // Create a new checksum
        $newChecksum = $this->filesystem->checksum($file);
        // Get the extension for the file
        $extension = pathinfo($file);
        $extension = $extension['extension'];
        // Get the checksum file
        $checksumFile = $checksumDir . $this->path . '.' . $extension;
        // Get the old checksum from the checksum file
        $oldChecksum = $this->filesystem->read($checksumFile);
        // Return true if it has changed, and false if it hasn't
        return $newChecksum !== $oldChecksum;
    }

    /**
     * Creates and returns a title based off of URL.
     *
     * @param string|null $file
     *
     * @return string
     */
    public static function getTitle (string $file = null): string {
        $config = Bootstrapper::getIni();
        $title = '';
        if (isset($_SERVER['REQUEST_URI'])) {
            // Get the request and defaultView
            $defaultView = $config['app']['default_view'] ?? 'index';
            $request = $_SERVER['REQUEST_URI'];

            // If the request is the same as the default view (or empty), return with the default view.
            if ($request === '/' || $request === '/'.$defaultView) {
                return ucfirst($defaultView);
            }

            // Add the request to the title variable for later
            $title .= $request;
        } elseif ($file !== null) {
            // If file != null, put the filename into the title variable, and remove the Extensions.
            $title = $file;
            $title .= str_replace([self::$mdExt, self::$htmlExt], '', $title);
        }

        // Remove any occurrence and everything after a forward slash and a question mark.
        $title = ltrim(urldecode($title),'/'); // Removes the last forward slash
        $title = preg_replace('/^.*\/\s*/', '', $title); // Removes all occurrences after a forward slash
        $title = preg_replace('/\?.*$/', '', $title); // Removes all occurrences after a question mark
        // Replace dashes and underscores with space
        $title = str_replace(['-','_'],' ',$title);

        // Return the newly formatted title, with the first character as uppercase.
        return ucfirst($title);
    }

    /**
     * Creates a menu based on a directory.
     *
     * @param $dir
     * @param bool $alternatingColours
     * @param int $cnt
     *
     * @return array
     *
     * @throws FilesystemException
     */
    public static function createMenu ($dir, bool $alternatingColours = true, int $cnt = 0): array {
        // Set some default variables
        $rootDir = Bootstrapper::rootDirectory();
        $mdPath = $rootDir.'/'.$dir;
        $adapter = new LocalFilesystemAdapter($rootDir.'/'.$dir);
        $filesystem = new Filesystem($adapter);
        $html = '';

        // If the directory exists, scan and loop through it, to print the menu based off of the contents in the directory.
        if ($filesystem->directoryExists('.')) {
            $sourceFiles = self::scanDirectory($mdPath);

            [$cnt, $html] = self::loopThroughFiles($dir, $sourceFiles, $alternatingColours, $cnt);
        }

        // We use this to keep track of alternating colours
        return [$cnt, $html];
    }

    /**
     * Loops through the information from createMenu method to craft the menu.
     *
     * @throws FilesystemException
     */
    private static function loopThroughFiles (
                                string $dir,
                                array $sourceFiles,
                                bool $alternatingColours,
                                int $cnt): array {
        // Loop through all files
        $html = '<ul>';
        $previousSourceFile = [];
        foreach ($sourceFiles as $key => $sourceFile) {
            // Whether the menu entry should have an alternating colour on it
            $altColourClass = '';
            if ($alternatingColours && $cnt % 2) {
                $altColourClass = 'class="isEven"';
            }

            // If this is a directory, add a menu entry and loop through it as well, so we have a recursive menu.
            if (!isset($sourceFile['name'])) {
                $linkText = str_replace(['_','-'], ' ', $key);
                // If there is a source file with the same name as the directory, link to it.
                if ($sourceFile['has_page'] !== false) {
                    $html .= "<li><a $altColourClass href='".$sourceFile['has_page']."'>$linkText</a>";
                }else {
                    $html .= "<li><a $altColourClass href='#'>$linkText</a>";
                }
                // Loop through the directory and create a nested "menu"
                [$cnt, $html2] = self::createMenu($dir.'/'.$key, $alternatingColours, $cnt+1);
                // Concat the result to $html
                $html .= $html2;
                $previousSourceFile = $sourceFile;
                continue;
            }

            // Check if the previous entry has the current element as a page, and continue if yes.
            if (isset($previousSourceFile['has_page']) && "/" . $sourceFile['name'] == $previousSourceFile['has_page']) {
                $cnt++;
                continue;
            }
            // Create a menu entry for the source file.
            $linkText = str_replace(['_','-'], ' ', $sourceFile['name']);
            $html .= "<li>
                        <a $altColourClass href='/{$sourceFile['parent']}{$sourceFile['name']}'>$linkText</a>
                      </li>";

            $cnt++;
            $previousSourceFile = $sourceFile;
        }
        $html .= "</ul>";
        // We use this to keep track of alternating colours
        return [$cnt, $html];
    }

    /**
     * Scans a specific directory; works based on the markdown path.
     *
     * @param string $path
     *
     * @return array[]
     */
    private static function scanDirectory (string $path): array {
        // Get the contents of the specified path.
        $contents = array_diff(scandir($path), ['.','..', '.git', '.gitignore', '.gitlab', 'uploads']);

        // Some default variables
        $resultStringed = [];
        $rootDir = Bootstrapper::rootDirectory();
        $ini = Bootstrapper::getIni();
        $mdPath = $rootDir.'/'.$ini['app']['md_path'];

        // Loop through the contents
        $counter = 0;
        foreach ($contents as $content) {
            // Get the full path
            $fullPath = $path.'/'.$content;
            // Get the "parent path" (path to the directory without the source file path and without the file/directory name)
            $parentPath = str_replace($mdPath, '', $fullPath);
            $parentPath = ltrim($parentPath, '/');
            // Get all off above without Extensions
            $contentNoExt = str_replace([self::$mdExt,self::$htmlExt], '', $content);
            $parentNoExt = str_replace([self::$mdExt,self::$htmlExt], '', $parentPath);
            // Remove the first forward slash from $parentNoExt
            $parentNoExt = preg_replace('/'. preg_quote($contentNoExt, '/') . '$/', '', $parentNoExt);

            // Check whether it is a directory
            if (is_dir($fullPath)) {
                // Scan it and add it to the sub_paths of both the named and counted array
                $resultStringed[$content]['sub_paths'] = self::scanDirectory($fullPath);
                // Check whether the directory has a document with the same name
                $resultStringed[$content]['has_page'] = self::dirHasPage($fullPath);
            } else {
                // If not a directory, just add it to the array
                $resultStringed[$content]['name'] = $contentNoExt;
                // Include the parent directories/documents
                $resultStringed[$content]['parent'] = $parentNoExt;
            }
            $counter++;
        }

        return $resultStringed;
    }

    /**
     * Checks whether a directory has a corresponding page to it.
     *
     * Returns $dir if yes, and false if not.
     *
     * @param string $dir
     *
     * @return false|string
     */
    private static function dirHasPage (string $dir): false|string {
        $rootDir = Bootstrapper::rootDirectory();
        $mdPath = Bootstrapper::getIni()['app']['md_path'];

        // Check whether a document with the same name as the directory supplied exists
        if (file_exists("$dir.md") || file_exists("$dir.html")) {
            // Return the name of the document if yes.
            return '/'.ltrim(str_replace([$rootDir, $mdPath], '', $dir), '/');
        }
        // Return false if not.
        return false;
    }

}
