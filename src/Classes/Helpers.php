<?php

declare(strict_types=1);

namespace Imms\Classes;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use TCB\FlysystemSync\Sync;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;

class Helpers {

    /**
     * Searches through all documents containing the search string
     *
     * @param $search
     *
     * @return void
     */
    public static function searchAjax ($search): void {
        // Setting the ini first is apparently required for the below to work.
        Bootstrapper::setIni();
        $config = Bootstrapper::getIni();

        $log = new Logger('Cache');
        $log->pushHandler(new StreamHandler($config['app']['log_path'], Level::Warning));

        // If search isn't an empty string
        if ($search !== '') {
            // Search through all markdown/HTML files for the search string
            $mdPath = $config['app']['md_path'] ?? 'src/documents/';
            // Define the search directory
            $searchDir = Bootstrapper::rootDirectory() . $mdPath;
            // Set up the filesystem
            $adapter = new LocalFilesystemAdapter($searchDir);
            $filesystem = new Filesystem($adapter);
            // Get a file list array
            $filesList = self::recursiveDirList($searchDir);
            $result = [];

            try {
                // Loop through each directory and file
                foreach ($filesList as $filename) {
                    // Remove the search directory from the file name and make it all lower case
                    $filename = str_replace($searchDir, '', $filename);
                    $fileContent = mb_strtolower($filesystem->read($filename));

                    // If the file contains what is being searched for
                    if (str_contains($fileContent, mb_strtolower($search))) {
                        // Remove dots from filename, and if the filename isn't empty, add a forward slash
                        $newFileName = str_replace('.', '', dirname($filename));
                        if (!empty($newFileName)) {
                            $newFileName = "$newFileName/";
                        }
                        // Get the basename of the filename, and store it in the result array
                        $newFileName .= basename(basename($filename, '.md'), '.html');
                        $result[] = $newFileName;
                    }
                }
            } catch (FilesystemException $e) {
                $log->error($e->getMessage());
                $log->error($e->getTraceAsString());
            }

            // Show the results
            echo '<div>';
            if ($result === []) {
                echo '<a>No results.</a>';
            } else {
                self::loopThroughResults($result);
            }
            echo '<div>';
        }
    }

    /**
     * Recursively creates a list (array) of the contents within a specified directory.
     *
     * This method is specifically created for the search bar.
     *
     * @param string $dir
     *
     * @return array
     */
    private static function recursiveDirList (string $dir): array {
        // Set and get the configuration, and store some variables.
        Bootstrapper::setIni();
        $ini = Bootstrapper::getIni();
        $rootDir = Bootstrapper::rootDirectory();
        $mdPath = $ini['app']['md_path'] ?? '/src/documents/';
        $searchDir = $rootDir . $mdPath;
        $dir = rtrim($dir, '/');
        $result = [];

        // Loop through the specified directory recursively
        foreach (glob($dir . '/*') as $file) {
            // If the file starts with "uploads", skip over it.
            if (str_starts_with($file, $searchDir.'uploads')) {
                continue;
            }

            // If file is a directory, loop through it also; else just put it in $result
            if (is_dir($file)) {
                $result = array_merge($result, self::recursiveDirList($file));
            } else {
                $result[] = $file;
            }
        }

        return $result;
    }

    /**
     * Reformats a result string that is in a subdirectory.
     *
     * @param string $resultString
     *
     * @return string
     */
    private static function formatSubDirResultString (string $resultString): string {
        $newResultString = '';
        // Explode the result string into an array.
        $rArray = explode('/', $resultString);
        // Loop through it, and construct the result string in a new way
        for ($i = 0; $i < count($rArray); $i++) {
            $rArray[$i] = ucfirst($rArray[$i]);
            if ($i === (count($rArray) - 1)) {
                $newResultString .= $rArray[$i];
            } else {
                $newResultString .= $rArray[$i].' → ';
            }
        }

        return $newResultString;
    }

    /**
     * Loops through the search results, and constructs links for them.
     *
     * @param string[] $results
     *
     * @return void
     */
    private static function loopThroughResults (array $results): void {
        // Loop through the supplied results
        foreach ($results as $result) {
            if (str_contains($result, '/')) {
                // Reformat the result string if it is in a subdirectory.
                $resultString = self::formatSubDirResultString($result);
            } else {
                // Capitalize first letter
                $resultString = ucfirst($result);
            }

            // Replace '-' and '_' with ' '
            $resultString = str_replace(['_','-'], ' ', $resultString);
            echo "<a onclick='$result' href='/$result'>$resultString</a>";
        }
    }

    /**
     * A simple sync method making use of FlySystem and an extension of it.
     *
     * @param string $from
     * @param string $to
     *
     * @return void
     */
    public static function sync (string $from, string $to): void {
        // Preparing the "from" adapter
        $masterAdapter = new LocalFilesystemAdapter($from);
        $master = new Filesystem($masterAdapter);
        // Preparing the "to" adapter
        $slaveAdapter = new LocalFilesystemAdapter($to);
        $slave = new Filesystem($slaveAdapter);

        Bootstrapper::setIni();
        $config = Bootstrapper::getIni();

        $log = new Logger('Cache');
        $log->pushHandler(new StreamHandler($config['app']['log_path'], Logger::WARNING));

        try {
            // If the count is different, synchronize.
            if (count($master->listContents("")->toArray()) !== count($slave->listContents("")->toArray())) {
                // Configure sync
                $sync = new Sync($master, $slave, ['visibility' => 'public', 'directory_visibility' => 'public'], '/');
                // Synchronise "from" to "to".
                $sync->sync();
            }
        } catch (FilesystemException $e) {
            $log->error($e->getMessage());
            $log->error($e->getTraceAsString());
        }
    }
}
