<?php

declare(strict_types=1);

namespace Imms\Classes;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ReflectionClass;
use ReflectionException;
use UnexpectedValueException;
use ScssPhp\ScssPhp\Compiler;
use League\Flysystem\Filesystem;
use Imms\Interfaces\ThemeInterface;
use League\Flysystem\FilesystemException;
use ScssPhp\ScssPhp\Exception\SassException;
use League\Flysystem\Local\LocalFilesystemAdapter;

class Theme {
    /**
     * Contains the IMMS config
     *
     * @var array|false
     */
    private static array|false $config;

    /**
     * Contains the root directory
     *
     * @var string|null
     */
    private static ?string $rootDir;

    /**
     * Contains the theme configuration
     *
     * @var array|false
     */
    private static array|false $themeConfig;

    /**
     * Contains the theme name
     *
     * @var string|null
     */
    private static ?string $theme;

    /**
     * Contains the base theme directory path
     *
     * @var string|null
     */
    private static ?string $baseThemeDir;
    private static string $themesDirNoRoot = '/src/themes/';
    private static Filesystem $themeFilesystem;
    private static Filesystem $cssCacheFilesystem;
    private static Logger $log;

    /**
     * Contains a theme object.
     *
     * @var ThemeInterface
     */
    public ThemeInterface $themeObject;

    /**
     * Theme handles themes in general.
     * When a new instance of Theme is created, it tries to install the theme if not already installed.
     */
    public function __construct() {
        self::$config = Bootstrapper::getIni();
        self::$rootDir = Bootstrapper::rootDirectory();
        self::$baseThemeDir = self::$rootDir.self::$themesDirNoRoot;
        self::$theme = self::$config['app']['theme'];
        self::$themeConfig = $this->getThemeConfig(self::$theme);
        $themeAdapter = new LocalFilesystemAdapter(self::$baseThemeDir);
        $cssCacheAdapter = new LocalFilesystemAdapter(self::$rootDir.'/web/assets/css');
        self::$themeFilesystem = new Filesystem($themeAdapter);
        self::$cssCacheFilesystem = new Filesystem($cssCacheAdapter);
        self::$log = new Logger('Cache');
        self::$log->pushHandler(new StreamHandler(self::$config['app']['log_path'], Logger::WARNING));
        // Save to $this->themeObject
        $this->instantiateAndSaveTheme(self::$theme);
    }

    /**
     * Creates an instance of a theme, and saves it to $templates
     *
     * @param string $theme
     *
     * @return void
     */
    private function instantiateAndSaveTheme(string $theme): void {
        // Split plugin name based on /
        $themeName = explode('/', $theme);

        // Get the theme namespace and fix the theme name
        $themeNamespace = explode('-', $themeName[1]);

        // Construct the namespace
        foreach ($themeNamespace as &$tns) {
            $tns = ucfirst($tns);
        }
        $themeName[1] = implode('', $themeNamespace);

        // Require the plugin (should probably have an autoloader for this?)
        require_once self::$rootDir.self::$themesDirNoRoot.$theme.'/'.$themeName[1].'.php';

        // Create the namespace of the plugin
        $theme = '\\'.$themeName[0].'\\'.$themeName[1].'\\'.$themeName[1];

        // Create a new reflection of the class and instantiate it as an object
        try {
            $objectReflection = new ReflectionClass($theme);
            try {
                $this->themeObject = $objectReflection->newInstance();
            } catch (ReflectionException $e) {
                echo "<p>Could not create a new instance of $theme! Perhaps not a valid theme?</p>";
                self::$log->error($e->getMessage());
                self::$log->error($e->getTraceAsString());
            }
        } catch (ReflectionException $e) {
            echo "<p>Could not create a new ReflectionClass of $theme! Perhaps not a valid theme?</p>";
            self::$log->error($e->getMessage());
            self::$log->error($e->getTraceAsString());
        }
    }

    /**
     * Returns the absolute path for a theme root.
     *
     * `$themeName` is expected to be "vendor/theme-name"
     *
     * @param string $themeName
     *
     * @return string
     */
    public static function getBaseThemePath (string $themeName): string {
        // Check whether the theme name is in the format "vendor/name"
        if (preg_match('/[A-Z0-9-_]*\/[A-Z0-9-_]*/im', $themeName) !== false) {
            // Return the full path to the theme root
            $rootDir = Bootstrapper::rootDirectory();
            return $rootDir.self::$themesDirNoRoot.$themeName;
        } else {
            $errorString = "Field 'themeName' MUST be in the format \"vendor/theme-name\"; \"$themeName\" given.";
            throw new UnexpectedValueException($errorString);
        }
    }

    /**
     * Compiles SCSS according to the configuration found in the config.ini file.
     *
     * @throws SassException
     * @throws FilesystemException
     */
    public function compileSCSS (): void {
        $themeConfig = self::$themeConfig;
        $themePath = self::$theme;
        if (isset($_COOKIE['IMMSTheme'])) {
            $themeConfig = $this->getThemeConfig($_COOKIE['IMMSTheme']);
            $themePath = $_COOKIE['IMMSTheme'];
        }
        // Compile SCSS
        $compiler = new Compiler();
        $cssPath = $themeConfig['theme']['compiled_stylesheet'] ?? 'main.css';
        $scssPath = $themePath.'/'.$themeConfig['theme']['scss_stylesheet'];
        $scssChecksumPath = $themePath.'/tmp/scss_checksum';
        if (!self::$themeFilesystem->fileExists($scssChecksumPath)) {
            self::$themeFilesystem->write($scssChecksumPath, "hogus bogus");
        }
        $oldChecksum = self::$themeFilesystem->read($scssChecksumPath);
        $newChecksum = self::$themeFilesystem->checksum($scssPath);

        if ($oldChecksum !== $newChecksum) {
            $compiler->SetImportPaths($this->getThemeLocation() . '/scss');
            // Compile the SCSS into CSS, and store it in a variable
            try {
                $scss = self::$themeFilesystem->read($scssPath);
                $css = $compiler->compileString($scss)->getCss();
                // Save it to the CSS file.
                self::$cssCacheFilesystem->write($cssPath, $css);
                self::$themeFilesystem->write($scssChecksumPath, $newChecksum);

            } catch (FilesystemException $e) {
                self::$log->error($e->getMessage());
                self::$log->error($e->getTraceAsString());
            }
        }
    }

    /**
     * Returns a string containing the theme location.
     *
     * @return string
     */
    private function getThemeLocation (): string {
        if (isset($_COOKIE['IMMSTheme'])) {
            return self::$baseThemeDir.$_COOKIE['IMMSTheme'];
        }
        return self::$baseThemeDir.self::$theme;
    }

    /**
     * Finds the theme configuration and returns it as an array.<br>
     * Dies with a message if the theme is not found.
     * The default theme (imms-dark) will be used if no theme name is supplied.
     *
     * @param string $themeName
     *
     * @return array
     */
    public static function getThemeConfig (string $themeName = 'inuitviking/imms-dark'): array {

        // See if the theme can be found and if yes return the theme.ini config.
        foreach (glob(self::$baseThemeDir.'*/*') as $theme) {
            if (str_contains($theme, $themeName)) {
                return parse_ini_file("$theme/theme.ini", true, INI_SCANNER_TYPED);
            }
        }
        die ("Theme $themeName not found!");
    }

    /**
     * Pre-renders a file, and returns its output.
     *
     * @param string $file
     *
     * @return bool|string
     */
    public static function prerender (string $file): bool|string {
        ob_start();
        require_once $file;
        return ob_get_clean();
    }
}
