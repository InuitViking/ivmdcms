<?php

declare(strict_types=1);

namespace Imms\Interfaces;

interface Mediator{
    /**
     * Notifies everyone subscribed to the specified event.
     * @param string $event
     * @return void
     */
    public function notifyToEvent (string $event): void;

    /**
     * Notifies a specified plugin of a specified event.
     * @param string $pluginName
     * @param string $event
     * @return void
     */
    public function notify (string $pluginName, string $event): void;
}