<?php

declare(strict_types=1);

// A couple of usings
use Imms\Classes\Bootstrapper;
use Imms\Classes\Helpers;

// I have no idea why this is needed
require_once '../../../src/Classes/Bootstrapper.php';

// But when the above line is in, everything works.
require_once Bootstrapper::rootDirectory() . '/vendor/autoload.php';
Helpers::searchAjax($_POST['search']);