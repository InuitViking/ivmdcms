<!-- omit in toc -->
# Contributing to IMMS

First off, thanks for taking the time to contribute!

All types of contributions are encouraged and valued. See the [Table of Contents](#table-of-contents) for different ways to help and details about how this project handles them. Please make sure to read the relevant section before making your contribution. It will make it a lot easier for us maintainers and smooth out the experience for all involved. The community looks forward to your contributions.

> And if you like the project, but just don't have time to contribute, that's fine. There are other easy ways to support the project and show your appreciation, which we would also be very happy about:
> - Star the project
> - Share it on social media
> - Refer this project in your project's readme
> - Mention the project at local meetups and tell your friends/colleagues

<!-- omit in toc -->
## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [I Have a Question](#i-have-a-question)
- [I Want To Contribute](#i-want-to-contribute)
- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Your First Code Contribution](#your-first-code-contribution)
- [Improving The Documentation](#improving-the-documentation)
- [Styleguides](#styleguides)
- [Commit Messages](#commit-messages)
- [Join The Project Team](#join-the-project-team)


## Code of Conduct

This project and everyone participating in it is governed by the
[IMMS Code of Conduct](https://gitlab.com/InuitViking/immsblob/master/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable behavior
to [contact-project+inuitviking-imms-34786769-issue-@incoming.gitlab.com](mailto:contact-project+inuitviking-imms-34786769-issue-@incoming.gitlab.com)
or create an issue.


## I Have a Question

> If you want to ask a question, we assume that you have read the available [Documentation](https://gitlab.com/InuitViking/imms/-/wikis/home).

Before you ask a question, it is best to search for existing [Issues](https://gitlab.com/InuitViking/imms/issues) that might help you. In case you have found a suitable issue and still need clarification, you can write your question in this issue. It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/InuitViking/imms/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions (PHP, composer, etc), depending on what seems relevant.
- Attach relevant labels to the issue (see below)

We will then take care of the issue as soon as possible.

### Labels

There are 8 labels to choose from:

- bug
  - Use this label if you think you've found a bug
- confirmed
  - This is used by a maintainer to underscore that something is confirmed as a bug
- critical
  - Use this label if the issue is a critical one (if everything is critical, nothing is; remember that)
- discussion
  - Use this label if the issue is just a discussion about something related to the project
  - This label can be changed to another one, such as `documentation` if relevant
- documentation
  - Use this label if the issue is about the documentation or is about improving and additions to the documentation 
- enhancement
  - Use this label if the issue / merge request is an enhancement to the project's code
- suggestion
  - Use this label if your issue is a suggestion to the code, documentation, or other aspects of the project
- support
  - Use this label if you're asking for support
  - Be aware that the maintainers only have limited time and access to support you directly
    and can only help you via the comments section on an issue; anything beyond that is not to be expected


## I Want To Contribute

> ### Legal Notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have authored 100% of the content, that you have the necessary rights to the content and that the content you contribute may be provided under the project license.

### Reporting Bugs

<!-- omit in toc -->
#### Before Submitting a Bug Report

A good bug report shouldn't leave others needing to chase you up for more information. Therefore, we ask you to investigate carefully, collect information and describe the issue in detail in your report. Please complete the following steps in advance to help us fix any potential bug as fast as possible.

- Make sure that you are using the latest version.
- Determine if your bug is really a bug and not an error on your side e.g. using incompatible environment components/versions (Make sure that you have read the [documentation](https://gitlab.com/InuitViking/imms/-/wikis/home). If you are looking for support, you might want to check [this section](#i-have-a-question)).
- To see if other users have experienced (and potentially already solved) the same issue you are having, check if there is not already a bug report existing for your bug or error in the [bug tracker](https://gitlab.com/InuitViking/immsissues?q=label%3Abug).
- Also make sure to search the internet (including Stack Overflow) to see if users outside the GitLab community have discussed the issue.
- Collect information about the bug:
  - Stack trace (Traceback)
  - OS, Platform and Version (Windows, Linux, macOS, x86, ARM)
  - Version of the interpreter, runtime environment, package manager, depending on what seems relevant.
  - Possibly your input and the output
  - Can you reliably reproduce the issue? And can you also reproduce it with older versions?

<!-- omit in toc -->
#### How Do I Submit a Good Bug Report?

> You must never report bugs containing sensitive information to the issue tracker; make sure to sanitise any and all information before creating an issue.
> Security related issues and vulnerabilities are welcome, and will be prioritised to fix ASAP.

We use GitLab issues to track bugs and errors. If you run into an issue with the project:

- Open an [Issue](https://gitlab.com/InuitViking/imms/issues/new).
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the *reproduction steps* that someone else can follow to recreate the issue on their own. This usually includes your code. For good bug reports you should isolate the problem and create a reduced test case.
- Please provide a list of installed composer packages as well.
- Provide the information you collected in the previous section.

Once it's filed:

- The project team will change the label on the issue if there is another more fitting label
- A team member will try to reproduce the issue with your provided steps. If there are no reproduction steps or no obvious way to reproduce the issue, the team will ask you for those steps and mark the issue as `needs-information`. Bugs with the `needs-information` tag will not be addressed until they are reproduced.
- If the team is able to reproduce the issue, it will be marked `needs-fix`, as well as possibly other tags (such as `critical`), and the issue will be left to be [implemented by someone](#your-first-code-contribution).


### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for IMMS, **including completely new features and minor improvements to existing functionality**. Following these guidelines will help maintainers and the community to understand your suggestion and find related suggestions.

<!-- omit in toc -->
#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Read the [documentation](https://gitlab.com/InuitViking/imms/-/wikis/home) carefully and find out if the functionality is already covered, maybe by an individual configuration.
- Perform a [search](https://gitlab.com/InuitViking/imms/issues) to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to convince the project's developers of the merits of this feature. Keep in mind that we want features that will be useful to the majority of our users and not just a small subset. If you're just targeting a minority of users, consider writing an add-on/plugin library.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?

Enhancement suggestions are tracked as [GitHub issues](https://gitlab.com/InuitViking/imms/issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion.
- Provide a **step-by-step description of the suggested enhancement** in as many details as possible.
- **Describe the current behavior** and **explain which behavior you expected to see instead** and why. At this point you can also tell which alternatives do not work for you.
- You may want to **include screenshots and animated GIFs** which help you demonstrate the steps or point out the part which the suggestion is related to. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast) or [this tool](https://github.com/GNOME/byzanz) on Linux.
- **Explain why this enhancement would be useful** to most IMMS users. You may also want to point out the other projects that solved it better and which could serve as inspiration.


### Your First Code Contribution

Have a look in the [Getting Started](README#getting-started) section in the README.

### Improving The Documentation

You can create an issue stating what part of the documentation should be updated, corrected, and/or improved, and with
what text. A diff could also be very helpful.

## Styleguides

### Commit Messages

Commit messages are expected to be formated something like below:

```
A title that is at most fifty characters long.

- Changes that are made
- In list form

Optionally add more information in a single paragraph.
Remember to add a "trailer" with one of the values below:

Changelog: added/fixed/updated/removed
```

## Join The Project Team

You must have contributed to the project before in the form of pull/merge requests, preferably regularly; once is not enough.

Once above criteria is made, you may make a new issue asking to join the team.

<!-- omit in toc -->
## Attribution

This guide is based on the **contributing-gen**. [Make your own](https://github.com/bttger/contributing-gen)!